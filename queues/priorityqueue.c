#include <stdio.h>
#define max 10
int front = -1, rear = -1, que[max];
insertion() {
  int data, i = 0, j = 0;
  printf("Enter data");
  scanf("%d", &data);

  if (rear >= max - 1) {
    printf("Over Flow!!\n");
    return;
  } else if (front == -1 && rear == -1) {
    front = 0;
    rear = 0;
    que[rear] = data;
    return;
  } else {
    for (i = 0; i <= rear; i++) {
      if (data >= que[i]) {
        for (j = rear + 1; j > i; j--) {
          que[j] = que[j - 1];
        }
        que[i] = data;
        break;
      }
    }
    que[i] = data;
  }
  rear = rear + 1;
}
deletion() {
  int i = 0, j = 0, item;
  if (front == -1) {
    printf("\nUNDER FLOW\n");
  }

  else {
    printf("\nEnter element to delete : ");
    scanf("%d", &item);
    for (i = front; i <= rear; i++) {
      if (item == que[i]) {
        while (i != rear + 1) {
          que[i] = que[i + 1];
          i++;
        }
      }
    }
  }
  rear = rear - 1;
}
display() {
  int i;
  for (i = front; i <= rear; i++) {
    printf("\n%d\n", que[i]);
  }
}

main() {
  int ch;
  printf("\n\n1.Insert\n2.Delete\n3.display\n\n\n");
  while (1) {
    printf("\n Enter Choice : ");
    scanf("%d", &ch);

    switch (ch) {
    case 1:
      insertion();
      break;
    case 2:
      deletion();
      break;
    case 3:
      display();
      break;
    }
  }
}
