#include <stdio.h>
#include <stdlib.h>
void push1(int);
void push2(int);
int pop1();
int pop2();
void enqueue();
void dequeue();
void display();
int st1[100], st2[100];
int top1 = -1, top2 = -1;
int count = 0, item;
void main() {
  int ch;
  printf("\n1 - Enqueue element into queue");
  printf("\n2 - Dequeu element from queue");
  printf("\n3 - Display from queue");
  printf("\n4 - Exit");
  while (1) {
    printf("\nEnter choice");
    scanf("%d", &ch);
    switch (ch) {
    case 1:
      enqueue();
      break;
    case 2:
      dequeue();
      break;
    case 3:
      display();
      break;
    case 4:
      exit(0);
    default:
      printf("Wrong choice");
    }
  }
}
void push1(int data) {
  top1 = top1 + 1;
  st1[top1] = data;
}
int pop1() {
  item = st1[top1];
  top1 = top1 - 1;
  return item;
}
void push2(int data) {
  top2 = top2 + 1;
  st2[top2] = data;
}

int pop2()

{
  item = st2[top2];
  top2 = top2 - 1;
  return item;
}

void enqueue()

{

  int data, i;

  printf("Enter data into queue");

  scanf("%d", &data);

  push1(data);

  count++;
}

void dequeue()

{

  int i;

  for (i = 0; i <= count; i++)

  {

    push2(pop1());
  }

  pop2();

  count--;

  for (i = 0; i <= count; i++)

  {

    push1(pop2());
  }
}

void display()

{
  int i;
  for (i = 0; i <= top1; i++) {
    printf(" %d ", st1[i]);
  }
}
