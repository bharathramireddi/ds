#include <stdio.h>
struct node {
  int data;
  int *link;
} *front = NULL, *rear = NULL;
void enq() {
  int item;
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter Data : ");
  scanf("%d", &item);
  temp->data = item;
  temp->link = NULL;
  if (rear == NULL) {
    front = temp;
    rear = temp;
  } else {
    rear->link = temp;
    rear = temp;
  }
}
int deq() {
  if (front == NULL) {
    printf("\nUnder Flow \n\n");
  } else {
    printf("\nElement Deleted : %d\n\n", front->data);
    if (front == rear) {
      front = NULL;
      rear = NULL;
    } else {
      front = front->link;
    }
  }
}
int display() {
  struct node *temp;
  temp = front;
  while (temp != NULL) {
    printf("\n%d ", temp->data);
    temp = temp->link;
  }
}

int main() {
  int ch;
  while (1) {
    printf("\n\tMENU\n1.ENQUEUE\n2.DEQUEUE\n3.DISPLAY\n4.EXIT\n");
    printf("\nEnter your choice\n");
    scanf("%d", &ch);

    switch (ch) {
    case 1:
      enq();
      break;
    case 2:
      deq();
      break;
    case 3:
      display();
      break;
    case 4:
      printf("Program Exited\n");
      exit(1);
    }
  }
}
