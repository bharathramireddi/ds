#include <stdio.h>
#define size 5
int queue[size];
int front = -1, rear = -1;
insert() {
  int data;
  printf("Enter Data : ");
  scanf("%d", &data);
  if (front == 0 && rear == size - 1 || front == rear + 1) {
    printf("\nOver Flow");
    return;
  } else if (rear == -1) {
    front++;
    rear++;
    queue[rear] = data;
  } else if (rear == size - 1 && front > 0) {
    rear = 0;
  } else {
    rear++;
  }
  queue[rear] = data;
}
delete () {
  int item;
  if (front == -1) {
    printf("Queue is empty");
  } else if (front == rear) {
    item = queue[front];
    front = -1;
    rear = -1;
    printf("\nDeleted-->%d", item);
  } else {
    item = queue[front];
    printf("\nDeleted-->%d", item);
    front = front + 1;
  }
}
display() {
  int i;
  if (front > rear) {
    for (i = front; i < size; i++) {
      printf("\n %d", queue[i]);
    }
    for (i = 0; i <= rear; i++) {
      printf("\n %d", queue[i]);
    }
  } else {
    for (i = front; i <= rear; i++) {
      printf("\n %d", queue[i]);
    }
  }
}
int main() {
  int ch;
  while (1) {
    printf("\n\n1.Insert\n2.Delete\n3.Display\n4.Exit\n\n");
    printf("\nEnter choice : ");
    scanf("%d", &ch);
    switch (ch) {

    case 1:
      insert();
      break;
    case 2:
      delete ();
      break;
    case 3:
      display();
      break;
    case 4:
      exit(1);
      break;
    }
  }
}
