#include <stdio.h>

struct node {
  int priority;
  int info;
  struct node *link;
} *front = NULL;

void insert(int item, int itempriority);
int del();
void display();
int isEmpty();

int main() {
  int ch, item, itempriority;
  while (1) {
    printf("\n1.Insert\n");
    printf("2.Delete\n");
    printf("3.Display\n");
    printf("4.Quit\n");
    printf("\nEnter your choice : ");
    scanf("%d", &ch);

    switch (ch) {
    case 1:
      printf("\nInput the item to be added in the queue : ");
      scanf("%d", &item);
      printf("\nEnter its priority : ");
      scanf("%d", &itempriority);
      insert(item, itempriority);
      break;
    case 2:
      printf("\nDeleted item is %d\n", del());
      break;
    case 3:
      display();
      break;
    case 4:
      exit(1);
    default:
      printf("\nWrong choice\n");
    }
  }

  return 0;
}

void insert(int item, int itempriority) {
  struct node *temp, *p;

  temp = (struct node *)malloc(sizeof(struct node));
  if (temp == NULL) {
    printf("\nMemory not available\n");
    return;
  }
  temp->info = item;
  temp->priority = itempriority;
  if (isEmpty() || itempriority < front->priority) {
    temp->link = front;
    front = temp;
  } else {
    p = front;
    while (p->link != NULL && p->link->priority <= itempriority)
      p = p->link;
    temp->link = p->link;
    p->link = temp;
  }
}

int del() {
  struct node *temp;
  int item;
  if (isEmpty()) {
    printf("\nQueue Underflow\n");
    exit(1);
  } else {
    temp = front;
    item = temp->info;
    front = front->link;
    free(temp);
  }
  return item;
}

int isEmpty() {
  if (front == NULL)
    return 1;
  else
    return 0;
}

void display() {
  struct node *ptr;
  ptr = front;
  if (isEmpty())
    printf("\nQueue is empty\n");
  else {
    printf("\nQueue is :\n");
    printf("\nPriority       Item\n");
    while (ptr != NULL) {
      printf("%d        %d\n", ptr->priority, ptr->info);
      ptr = ptr->link;
    }
  }
}
