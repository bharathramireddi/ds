#include <stdio.h>
Sparse(int matrix[3][3], int row, int col) {
  int i, j, count;
  for (i = 0; i < row; i++) {
    for (j = 0; j < col; j++) {
      if (matrix[i][j] == '0') {
        count++;
      }
    }
  }
  if (count > row * col / 2) {
    printf("\nSparse matrix\n");
  } else {
    printf("\nNot a Sparse matrix\n");
  }
}

int main() {
  int matrix[3][3] = {1, 2, 0, 0, 0, 0, 0, 8, 9};

  Sparse(matrix, 3, 3);
}
