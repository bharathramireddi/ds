#include <stdio.h>
struct node {
  int data;
  int *link;
};
struct node *start;
struct node *start1;
void create()

{
  int data, n, i;
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\n Enter the data:");
  scanf("%d", &temp->data);
  temp->link = temp;
  start = temp;
}
void insertbegin() {
  int data;
  struct node *temp;
  struct node *p;
  p = start;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter data : ");
  scanf("%d", &temp->data);
  while (p->link != start) {
    p = p->link;
  }
  p->link = temp;
  temp->link = start;
  start = temp;
}
traverse() {
  struct node *p;
  p = start;
  while (p->link != start) {
    printf("%d->", p->data);
    p = p->link;
  }
  printf("%d", p->data);
}

void insertatend() {
  struct node *temp;
  struct node *p;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter data : ");
  scanf("%d", &temp->data);
  p = start;
  while (p->link != start) {
    p = p->link;
  }
  p->link = temp;
  temp->link = start;
}
void insertafter() {
  int i = 1, loc;
  struct node *temp;
  struct node *p;
  p = start;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter Loc : ");
  scanf("%d", &loc);
  printf("\nEnter Data :");
  scanf("%d", &temp->data);
  while (i < loc) {
    p = p->link;
    i++;
  }
  temp->link = p->link;
  p->link = temp;
}
void modify() {
  int data, mod;
  struct node *p;
  p = start;
  traverse();
  printf("\nEnter Node to Modify :");
  scanf("%d", &data);
  printf("\nEnter Modified data : ");
  scanf("%d", &mod);
  while (p->link != start) {
    if (p->data == data) {
      p->data = mod;
      printf("\nNode matched and modified !! ");
    }
    p = p->link;
  }
  if (p->data == data) {
    p->data = mod;
    printf("\nNode matched and modified !! ");
  }
}

search() {
  int data, loc = 1;
  struct node *p;
  p = start;
  traverse();
  printf("\nEnter Node to search :");
  scanf("%d", &data);
  while (p->link != start) {
    if (p->data == data) {
      printf("\nNode matched at %d ", loc);
    }
    loc++;
    p = p->link;
  }
  if (p->data == data) {
    printf("\nNode matched  at %d", loc);
  }
}
void create2() {
  int data, c = 1;
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\n Enter the data:");
  scanf("%d", &temp->data);
  temp->link = temp;
  start1 = temp;
  struct node *p;
  while (c) {
    temp = (struct node *)malloc(sizeof(struct node));
    printf("\nEnter data : ");
    scanf("%d", &temp->data);
    p = start1;
    while (p->link != start1) {
      p = p->link;
    }
    p->link = temp;
    temp->link = start1;
    printf("Enter 0 to stop");
    scanf("%d", &c);
  }
}

int traverse2() {
  struct node *p;
  p = start1;
  while (p->link != start1) {
    printf("%d->", p->data);
    p = p->link;
  }
  printf("%d", p->data);
}

void concate() {
  struct node *p;
  struct node *q;
  p = start;
  while (p->link != start) {
    p = p->link;
    if (p->link == start) {
      p->link = start1;
      q = start1;
      while (q->link != start1) {
        q = q->link;
      }
      q->link = start;
      break;
    }
  }
}

reverse() {
  struct node *temp;
  struct node *prev;
  struct node *cur;
  temp = start;
  prev = start;
  start = start->link;
  cur = start;
  while (start != temp) {
    start = start->link;
    cur->link = prev;
    prev = cur;
    cur = start;
  }
  cur->link = prev;
  start = prev;
  traverse();
}
maxmin() {
  int max, min;

  struct node *p;
  struct node *q;
  p = start->link;
  max = start->data;
  min = start->data;
  while (p != start) {
    if (p->data > max) {
      max = p->data;
    }
    p = p->link;
  }
  q = start->link;
  while (q != start) {
    if (min > q->data) {
      min = q->data;
    }
    q = q->link;
  }

  printf("\nMax-->%d\nMin-->%d\n\n", max, min);
}

int main() {

  int ch;

  while (1) {
    printf("\n1.Create\n2.Insert at Begin\n3.Traverse\n4.insert at "
           "end\n5.insert after\n6.Modify\n");
    printf("7.Search\n8.Create List 2\n9.Traverse List "
           "\n10.Concate\n11.Reverse\n12.Man And Min\n\n");
    printf("Choose one : ");
    scanf("%d", &ch);
    switch (ch) {
    case 1:
      create();
      break;
    case 2:
      insertbegin();
      break;
    case 3:
      traverse();
      break;
    case 4:
      insertatend();
      break;
    case 5:
      insertafter();
      break;
    case 6:
      modify();
      break;
    case 7:
      search();
      break;
    case 8:
      create2();
      break;
    case 9:
      traverse2();
      break;
    case 10:
      concate();
      break;
    case 11:
      reverse();
      break;
    case 12:
      maxmin();
      break;
    }
    printf("\n\n\n");
  }
}
