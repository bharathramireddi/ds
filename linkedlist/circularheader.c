#include <stdio.h>
struct node {
  int data;
  struct node *link;
};
struct node *start;
struct node *start1;
void create(struct node *start)

{
  int data, n, i;
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\n Enter the data:");
  scanf("%d", &temp->data);
  temp->link = start;
  start->link = temp;
  start->data = 10;
  printf("\nEnter No of nodes : ");
  scanf("%d", &n);
  for (i = 0; i < n; i++) {
    insertatend(start);
  }
}
void insertbegin(struct node *start) {
  int data;
  struct node *temp;
  struct node *p;
  p = start->link;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter data : ");
  scanf("%d", &temp->data);
  /*  while (p->link != start) {
      p = p->link;
    }
    p->link = start;*/
  temp->link = start->link;
  start->link = temp;
}
traverse(struct node *start) {
  struct node *p;
  p = start;
  while (p->link != start) {
    printf("%d->", p->data);
    p = p->link;
  }
  printf("%d", p->data);
}

void insertatend(struct node *start) {
  struct node *temp;
  struct node *p;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter data : ");
  scanf("%d", &temp->data);
  p = start->link;
  while (p->link != start) {
    p = p->link;
  }
  p->link = temp;
  temp->link = start;
}
void insertafter(struct node *start) {
  int i = 1, loc;
  struct node *temp;
  struct node *p;
  p = start->link;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter Loc : ");
  scanf("%d", &loc);
  printf("\nEnter Data :");
  scanf("%d", &temp->data);
  while (i < loc) {
    p = p->link;
    i++;
  }
  temp->link = p->link;
  p->link = temp;
}
void modify(struct node *start) {
  int data, mod;
  struct node *p;
  p = start;
  traverse(start);
  printf("\nEnter Node to Modify :");
  scanf("%d", &data);
  printf("\nEnter Modified data : ");
  scanf("%d", &mod);
  while (p->link != start) {
    if (p->data == data) {
      p->data = mod;
      printf("\nNode matched and modified !! ");
    }
    p = p->link;
  }
  if (p->data == data) {
    p->data = mod;
    printf("\nNode matched and modified !! ");
  }
}

search(struct node *start) {
  int data, loc = 1;
  struct node *p;
  p = start->link;
  traverse(start);
  printf("\nEnter Node to search :");
  scanf("%d", &data);
  while (p->link != start) {
    if (p->data == data) {
      printf("\nNode matched at %d ", loc);
    }
    loc++;
    p = p->link;
  }
  if (p->data == data) {
    printf("\nNode matched  at %d", loc);
  }
}

void concate(struct node *start, struct node *start1) {
  struct node *p;
  struct node *q;
  p = start->link;
  while (p->link != start) {
    p = p->link;
    if (p->link == start) {
      p->link = start1->link;
      q = start1;
      while (q->link != start1) {
        q = q->link;
      }
      q->link = start;
      break;
    }
  }
}

reverse(struct node *start) {
  struct node *temp;
  struct node *prev;
  struct node *cur;
  start = start->link;
  temp = start;
  prev = start;
  start = start->link;
  cur = start;
  while (start != temp) {
    start = start->link;
    cur->link = prev;
    prev = cur;
    cur = start;
  }
  cur->link = prev;
  start = prev;
  traverse(start);
}
maxmin(struct node *start) {
  int max, min;
  struct node *temp;
  temp = start;
  struct node *p;
  struct node *q;
  start = start->link;
  p = start;
  max = start->data;
  min = start->data;
  while (p->link != temp) {
    p = p->link;
    if (p->data > max) {
      max = p->data;
    }
  }
  q = start->link;
  while (q != start) {
    if (min > q->data) {
      min = q->data;
    }
    q = q->link;
  }

  printf("\nMax-->%d\nMin-->%d\n\n", max, min);
}

int main() {

  int ch;
  struct node *head;
  struct node *head2;
  head = (struct node *)malloc(sizeof(struct node));
  head2 = (struct node *)malloc(sizeof(struct node));

  while (1) {
    printf("\n1.Create\n2.Insert at Begin\n3.Traverse\n4.insert at "
           "end\n5.insert after\n6.Modify\n");
    printf("7.Search\n8.Create List 2\n9.Traverse List "
           "\n10.Concate\n11.Reverse\n12.Man And Min\n\n");
    printf("Choose one : ");
    scanf("%d", &ch);
    switch (ch) {
    case 1:
      create(head);
      break;
    case 2:
      insertbegin(head);
      break;
    case 3:
      traverse(head);
      break;
    case 4:
      insertatend(head);
      break;
    case 5:
      insertafter(head);
      break;
    case 6:
      modify(head);
      break;
    case 7:
      search(head);
      break;
    case 8:
      create(head2);
      break;
    case 9:
      traverse(head2);
      break;
    case 10:
      concate(head, head2);
      break;
    case 11:
      reverse(head);
      break;
    case 12:
      maxmin(head);
      break;
    }
    printf("\n\n\n");
  }
}
