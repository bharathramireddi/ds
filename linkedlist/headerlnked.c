#include <stdio.h>
#include <stdlib.h>

struct node {
  int info;
  struct node *link;
};

struct node *create_list(struct node *head);
void display(struct node *head);
struct node *addatend(struct node *head, int data);
struct node *addbefore(struct node *head, int data, int item);
struct node *addatpos(struct node *head, int data, int pos);
struct node *search(struct node *head, int data);
struct node *reverse(struct node *head);
struct node *modify(struct node *head, int data);
struct node *concat(struct node *head, struct node *head2);

main() {
  int choice, data, item, pos;
  struct node *head;
  struct node *head2;
  head = (struct node *)malloc(sizeof(struct node));
  head = create_list(head);
  head2 = (struct node *)malloc(sizeof(struct node));
  while (1) {

    printf("1.Display\n");
    printf("2.insert at end\n");
    printf("3.insert at begin\n");
    printf("4.insert at loc\n");
    printf("5.Search\n");
    printf("6.Reverse\n");
    printf("7.Modify\n8.CreateList2\n9.Concat\n\n");
    printf("Enter your choice : ");
    scanf("%d", &choice);
    switch (choice) {
    case 1:
      display(head);
      break;
    case 2:
      printf("Enter the element to be inserted : ");
      scanf("%d", &data);
      head = addatend(head, data);
      break;
    case 3:
      printf("Enter the element to be inserted : ");
      scanf("%d", &data);
      printf("Enter the element before which to insert : ");
      scanf("%d", &item);
      head = addbefore(head, data, item);
      break;
    case 4:
      printf("Enter the element to be inserted : ");
      scanf("%d", &data);
      printf("Enter the position at which to insert : ");
      scanf("%d", &pos);
      head = addatpos(head, data, pos);
      break;
    case 5:
      printf("Enter the element to Searh : ");
      scanf("%d", &data);
      search(head, data);
      break;
    case 6:
      head = reverse(head);
      break;
    case 7:
      printf("Enter element to modify : ");
      scanf("%d", &data);
      head = modify(head, data);
      break;
    case 8:
      head2 = create_list(head2);
      break;
    case 9:
      head = concat(head, head2);
      break;
    }
  }
}

struct node *create_list(struct node *head) {
  int i, n, data;
  printf("Enter the number of nodes : ");
  scanf("%d", &n);
  for (i = 1; i <= n; i++) {
    printf("Enter the element to be inserted : ");
    scanf("%d", &data);
    head = addatend(head, data);
  }
  return head;
}

void display(struct node *head) {
  struct node *p;
  if (head->link == NULL) {
    printf("List is empty\n");
    return;
  }
  p = head;
  printf("List is :\n");
  while (p->link != NULL) {
    p = p->link;
    printf("%d ", p->info);
  }
  printf("\n");
}

struct node *addatend(struct node *head, int data) {
  struct node *p, *tmp;
  tmp = (struct node *)malloc(sizeof(struct node));
  tmp->info = data;
  p = head;
  while (p->link != NULL)
    p = p->link;
  p->link = tmp;
  tmp->link = NULL;
  return head;
}

struct node *addbefore(struct node *head, int data, int item) {
  struct node *tmp, *p;
  p = head;
  while (p->link != NULL) {
    if (p->link->info == item) {
      tmp = (struct node *)malloc(sizeof(struct node));
      tmp->info = data;
      tmp->link = p->link;
      p->link = tmp;
      return head;
    }
    p = p->link;
  }
  printf("%d not present in the list\n", item);
  return head;
}

struct node *addatpos(struct node *head, int data, int pos) {
  struct node *tmp, *p;
  int i;
  tmp = (struct node *)malloc(sizeof(struct node));
  tmp->info = data;
  p = head;
  for (i = 1; i <= pos - 1; i++) {
    p = p->link;
    if (p == NULL) {
      printf("There are less than %d elements\n", pos);
      return head;
    }
  }
  tmp->link = p->link;
  p->link = tmp;
  return head;
}

struct node *search(struct node *head, int data) {
  struct node *tmp, *p;
  p = head;
  int loc = 0;
  while (p->link != NULL) {
    loc++;
    p = p->link;
    if (p->info == data) {
      printf("Element present at %d \n", loc);
      return head;
    }
  }
  printf("Element %d not found\n", data);
  return head;
}

struct node *reverse(struct node *head) {
  struct node *prev, *ptr, *next;
  prev = NULL;
  ptr = head->link;
  while (ptr != NULL) {
    next = ptr->link;
    ptr->link = prev;
    prev = ptr;
    ptr = next;
  }
  head->link = prev;
  return head;
}
struct node *modify(struct node *head, int data) {
  struct node *p;
  p = head;
  while (p->link != NULL) {
    p = p->link;
    if (p->info == data) {
      printf("Enter modied data : ");
      scanf("%d", &p->info);
      return head;
    }
  }
  return head;
}

struct node *concat(struct node *head, struct node *head2) {
  struct node *p;
  p = head;
  while (p->link != NULL) {
    p = p->link;
    if (p->link == NULL) {
      if (p->link == 0) {
        head2 = head2->link;
      }
      p->link = head2;
      break;
    }
  }
  return head;
}
