#include <stdio.h>
#include <stdlib.h>
struct node {
  struct node *prev;
  int data;
  struct node *next;
};
void create(struct node *start) {
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter Data :");
  scanf("%d", &temp->data);
  start->data = 10;
  start->next = temp;
  temp->prev = start;
  temp->next = start;
  start->prev = temp;
}
display(struct node *start) {
  struct node *p;
  printf("%d->", start->data);
  p = start->next;
  while (p->next != start) {
    printf("%d->", p->data);
    p = p->next;
  };
  printf("%d", p->data);
}
insertatbeg(struct node *start) {
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nenter data :");
  scanf("%d", &temp->data);
  temp->next = start->next;
  start->next->prev = temp;
  start->next = temp;
  temp->prev = start;
}
insertatend(struct node *start) {
  struct node *temp;
  struct node *p;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter data at end :");
  scanf("%d", &temp->data);
  p = start->next;
  while (p->next != start) {
    p = p->next;
  }
  p->next = temp;
  temp->prev = p;
  temp->next = start;
  start->prev = temp;
}
insertafter(struct node *start) {
  struct node *temp;
  struct node *p;
  int i = 1, loc;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter data : ");
  scanf("%d", &temp->data);
  printf("\nEnter location : ");
  scanf("%d", &loc);
  p = start->next;
  while (i < loc) {
    p = p->next;
    i++;
  }

  temp->next = p->next;
  p->next = temp;
  temp->prev = p;
  p = p->next;
  p->prev = temp;
}

modify(struct node *start) {
  struct node *temp;
  struct node *p;
  int i = 1, loc;
  temp = (struct node *)malloc(sizeof(struct node));
  printf("\nEnter location : ");
  scanf("%d", &loc);
  p = start->next;
  while (i < loc) {
    p = p->next;
    i++;
  }
  printf("\nEnter modified : ");
  scanf("%d", &p->data);
}
reverse(struct node *start) {
  struct node *p;
  p = start;
  printf("%d-->", start->data);
  while (p->next != start) {
    p = p->next;
  }
  printf("%d->", p->data);
  while (p->prev != start) {
    p = p->prev;
    printf("%d->", p->data);
  }
}

main() {
  int ch;
  struct node *head;
  head = (struct node *)malloc(sizeof(struct node));
  while (1) {
    printf("\n1.Create\n2.Insert at Begin\n3.Insertat End\n4.Insert "
           "after\n5.Modify\n6.reverse\n7.display\n8.exit\n\n");
    printf("\nEnter choice : ");
    scanf("%d", &ch);

    switch (ch) {
    case 1:
      create(head);
      break;
    case 2:
      insertatbeg(head);
      break;
    case 3:
      insertatend(head);
      break;
    case 4:
      insertafter(head);
      break;
    case 5:
      modify(head);
      break;
    case 6:
      reverse(head);
      break;
    case 7:
      display(head);
      break;
    case 8:
      exit(1);
    }
  }
}
