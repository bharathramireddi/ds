#include <stdio.h>
#include <stdlib.h>

struct node {
  int data;
  int *next;
};

struct node *start = NULL;
struct node *start1 = NULL;
void insert_at_begin(int);
void insert_at_end(int);
void traverse();
void insertafter();
int search();
void modify();
void create();
void concate();
void traverse1();
int maxmin();
void reverse();
int count = 0;

int main() {
  int ch, data;

  while (1) {
    printf("1. Insert at beginning\n");
    printf("2. Insert at end\n");
    printf("3. Traverse\n");
    printf("4. Insert after\n");
    printf("5. Search\n");
    printf("6. Modify\n");
    printf("7.Create new linkedlist\n");
    printf("8.traverse list2\n9.Concate\n10.Max..Min\n11.Reverse\n");
    scanf("%d", &ch);

    switch (ch) {
    case 1:
      printf("Enter value of element\n");
      scanf("%d", &data);
      insert_at_begin(data);
      break;
    case 2:
      printf("Enter value of element\n");
      scanf("%d", &data);
      insert_at_end(data);
      break;
    case 3:
      traverse();
      break;
    case 4:
      insertafter();
      break;

    case 5:
      search();
      break;
    case 6:
      modify();
      break;
    case 7:
      create();
      break;
    case 8:
      traverse1();
      break;
    case 9:
      concate();
      break;
    case 10:
      maxmin();
      break;
    case 11:
      reverse();
      break;
    }
  }
}

void insert_at_begin(int x) {
  struct node *temp;

  temp = (struct node *)malloc(sizeof(struct node));
  count++;

  if (start == NULL) {
    start = temp;
    start->data = x;
    start->next = NULL;
    return;
  }

  temp->data = x;
  temp->next = start;
  start = temp;
}

void insert_at_end(int x) {
  struct node *temp, *p;

  temp = (struct node *)malloc(sizeof(struct node));
  count++;

  if (start == NULL) {
    start = temp;
    start->data = x;
    start->next = NULL;
    return;
  }

  p = start;

  while (p->next != NULL) {
    p = p->next;
  }
  p->next = temp;
  temp->data = x;
  temp->next = NULL;
}

void traverse() {
  struct node *p;

  p = start;

  if (p == NULL) {
    printf("\nLinked list Empty.\n");
    return;
  }

  printf("Elements are : \n\n");

  while (p->next != NULL) {
    printf("%d->", p->data);
    p = p->next;
  }
  printf("%d\n\n", p->data);
}
void insertafter() {
  int i = 1, loc;
  struct node *p;
  struct node *temp;
  temp = (struct node *)malloc(sizeof(struct node *));
  printf("\nEnter Location : ");
  scanf("%d", &loc);
  if (loc > count) {
    printf("\nWrong Location\n");
  } else {
    printf("Enter Data : ");
    scanf("%d", &temp->data);
    p = start;
    while (i < loc) {
      p = p->next;
      i++;
    }
    temp->next = p->next;
    p->next = temp;
  }
  count++;
}
int search() {
  int loc = 1, data;
  struct node *p;
  p = start;
  printf("\nEnter data to search : ");
  scanf("%d", &data);
  while (p->next != NULL) {
    p = p->next;
    loc++;
    if (p->data == data) {
      printf("Location--->%d\n", loc);

      break;
    }
  }
  printf("\nNode Not Present in list \n");
}

void modify() {
  int data, mod;
  struct node *p;
  p = start;
  printf("\nEnter node to modify : ");
  scanf("%d", &data);
  while (p->next != NULL) {
    p = p->next;
    if (p->data == data) {
      printf("\nNode matched!!\nEnter Data to modify : ");
      scanf("%d", &mod);
      p->data = mod;
    }
  }
  printf("Modified\n\n");
}

void concate() {
  int data;
  struct node *p;
  p = start;
  while (p->next != NULL) {
    p = p->next;
    if (p->next == NULL) {
      p->next = start1;
      break;
    }
  }
  printf("Concated\n\n");

  traverse();
}

void create() {
  struct node *temp;
  struct node *q = NULL;
  int i = 1, n;
  temp = (struct node *)malloc(sizeof(struct node));
  if (start1 == NULL) {
    start1 = temp;
    start->next == NULL;
  }
  q = start1;

  while (q->next != NULL) {
    q = q->next;
  }
  printf("\nEnter data : ");
  scanf("%d", &temp->data);
  q->next = temp;
  temp->next = NULL;
}
void traverse1() {
  struct node *p;

  p = start1;

  if (p == NULL) {
    printf("\nLinked list Empty.\n");
    return;
  }

  printf("Elements are : \n\n");

  while (p->next != NULL) {
    printf("%d->", p->data);
    p = p->next;
  }
  printf("%d\n\n", p->data);
}

int maxmin() {
  int max = -999, min = 999;

  struct node *p;
  struct node *q;
  p = start;
  while (p != NULL) {
    if (p->data > max) {
      max = p->data;
    }
    p = p->next;
  }
  q = start;
  while (q != NULL) {
    if (min > q->data) {
      min = q->data;
    }
    q = q->next;
  }

  printf("\nMax-->%d\nMin-->%d\n\n", max, min);
}

void reverse() {
  struct node *cur;
  struct node *prev;
  if (start != NULL) {
    prev = start;
    start = start->next;
    cur = start;
    prev->next = NULL;
    while (start != NULL) {
      start = start->next;
      cur->next = prev;
      prev = cur;
      cur = start;
    }
    start = prev;
    printf("Reversed \n");
    traverse();
  }
}
