
#include <stdio.h>
#include <stdlib.h>

struct node {
  struct node *prev;
  int n;
  struct node *next;
} * h, *temp, *temp1, *temp2, *temp4;

void insertbeg();
void insertend();
void insertatpos();
void traverse();
void search();
void update();
void reverse();

int count = 0;

void main() {
  int ch;

  h = NULL;
  temp = temp1 = NULL;

  printf("\n 1 - Insert at beginning");
  printf("\n 2 - Insert at end");
  printf("\n 3 - Insert at position i");
  printf("\n 4 - Reverse");
  printf("\n 5 - Display from beginning");
  printf("\n 6 - Search for element");
  printf("\n 7 - Update an element");
  printf("\n 8 - Exit");

  while (1) {
    printf("\n Enter choice : ");
    scanf("%d", &ch);
    switch (ch) {
    case 1:
      insertbeg();
      break;
    case 2:
      insertend();
      break;
    case 3:
      insertatpos();
      break;
    case 4:
      reverse();
      break;
    case 5:
      traverse();
      break;
    case 6:
      search();
      break;
    case 7:
      update();
      break;
    case 8:
      exit(0);
    default:
      printf("\n Wrong choice menu");
    }
  }
}

void create() {
  int data;

  temp = (struct node *)malloc(1 * sizeof(struct node));
  temp->prev = NULL;
  temp->next = NULL;
  printf("\n Enter value to node : ");
  scanf("%d", &data);
  temp->n = data;
  count++;
}

void insertbeg() {
  if (h == NULL) {
    create();
    h = temp;
    temp1 = h;
  } else {
    create();
    temp->next = h;
    h->prev = temp;
    h = temp;
  }
}

void insertend() {
  if (h == NULL) {
    create();
    h = temp;
    temp1 = h;
  } else {
    create();
    temp1->next = temp;
    temp->prev = temp1;
    temp1 = temp;
  }
}

void insertatpos() {
  int pos, i = 2;

  printf("\n Enter position to be inserted : ");
  scanf("%d", &pos);
  temp2 = h;

  if ((pos < 1) || (pos >= count + 1)) {
    printf("\n Position out of range to insert");
    return;
  }
  if ((h == NULL) && (pos != 1)) {
    printf("\n Empty list cannot insert other than 1st position");
    return;
  }
  if ((h == NULL) && (pos == 1)) {
    create();
    h = temp;
    temp1 = h;
    return;
  } else {
    while (i < pos) {
      temp2 = temp2->next;
      i++;
    }
    create();
    temp->prev = temp2;
    temp->next = temp2->next;
    temp2->next->prev = temp;
    temp2->next = temp;
  }
}

void traverse() {
  temp2 = h;

  if (temp2 == NULL) {
    printf("List empty to display \n");
    return;
  }
  printf("\n Linked list elements from begining : ");

  while (temp2->next != NULL) {
    printf(" %d ", temp2->n);
    temp2 = temp2->next;
  }
  printf(" %d ", temp2->n);
}

void search() {
  int data, count = 0;
  temp2 = h;

  if (temp2 == NULL) {
    printf("\n Error : List empty to search for data");
    return;
  }
  printf("\n Enter value to search : ");
  scanf("%d", &data);
  while (temp2 != NULL) {
    if (temp2->n == data) {
      printf("\n Data found in %d position", count + 1);
      return;
    } else
      temp2 = temp2->next;
    count++;
  }
  printf("\n Error : %d not found in list", data);
}

void update() {
  int data, data1;

  printf("\n Enter node data to be updated : ");
  scanf("%d", &data);
  printf("\n Enter new data : ");
  scanf("%d", &data1);
  temp2 = h;
  if (temp2 == NULL) {
    printf("\n Error : List empty no node to update");
    return;
  }
  while (temp2 != NULL) {
    if (temp2->n == data) {

      temp2->n = data1;
      traverse();
      return;
    } else
      temp2 = temp2->next;
  }

  printf("\n Error : %d not found in list to update", data);
}

void reverse() {
  struct node *p;
  p = h;
  while (p->next != NULL) {
    p = p->next;
  }
  printf("\n  Reverse Order : \n");
  printf(" %d ", p->n);
  while (p->prev != NULL) {
    p = p->prev;
    printf(" %d ", p->n);
  }
  printf("\n");
  traverse();
}
