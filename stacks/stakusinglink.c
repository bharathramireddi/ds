#include <stdio.h>

void push();

void pop();

void display();

struct node {

  int data;

  int *link;
};

struct node *top = NULL;

int main() {
  int ch;

  while (1) {

    printf("\n\n\n1.Push\n2.Pop\n3.Display\n\n");

    scanf("%d", &ch);

    switch (ch) {

    case 1:
      push();

      break;

    case 2:
      pop();

      break;

    case 3:
      display();

      break;
    }
  }
}

void push() {
  struct node *temp;

  temp = (struct node *)malloc(sizeof(struct node));

  printf("Enter DATA : ");

  scanf("%d", &temp->data);

  temp->link = top;

  top = temp;
}

void pop() {

  if (top == NULL) {

    printf("Stack is empty");

  } else {
    struct node *temp;

    temp = top;

    printf("Popped element is: %d\n", temp->data);

    top = top->link;

    temp->link = NULL;

    free(temp);
  }
}

void display() {

  struct node *temp;

  printf("Elementd in stack are : ");

  temp = top;

  while (temp != NULL) {

    printf("\n%d\n", temp->data);

    temp = temp->link;
  }
}
