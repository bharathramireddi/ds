#include <stdio.h>
#define size 10
int top1 = -1, top2 = size, data;
char stack[size];
void push1() {
  if (top1 == top2) {
    printf("Over Flow\n");
  } else {
    printf("\nEnter Data to push in stack 1: ");
    scanf("%d", &data);
    top1 = top1 + 1;
    stack[top1] = data;
  }
}
char pop1() {
  char popped;
  if (top1 == -1) {
    printf("Under FLOW");
  } else {
    popped = stack[top1];
    top1 = top1 - 1;
    printf("%d\n", popped);
  }
}
void push2() {
  if (top2 == top1) {
    printf("Over Flow\n");
  } else {
    printf("\nEnter Data to Push in stack 2: ");
    scanf("%d", &data);
    top2 = top2 - 1;
    stack[top2] = data;
  }
}
char pop2() {
  char popped;
  if (top2 == size) {
    printf("Under Flow\n");
  }

  else {
    popped = stack[top2];
    top2 = top2 + 1;
    printf("%d\n", popped);
  }
}
display1() {
  int i;
  for (i = top1; i >= 0; i--) {
    printf("\n%d\n", stack[i]);
  }
}
display2() {
  int i;
  for (i = top2; i < size; i++) {
    printf("\n%d\n", stack[i]);
  }
}
int main() {
  int ch;
  while (1) {
    printf("\n1.PUSH1\n2.Push2\n3.Pop1\n4.Pop2\n5.Display1\n6.Display2\n\n");
    printf("Entered Option : ");
    scanf("%d", &ch);
    switch (ch) {
    case 1:
      push1();
      break;
    case 2:
      push2();
      break;
    case 3:
      pop1();
      break;
    case 4:
      pop2();
      break;
    case 5:
      display1();
      break;
    case 6:
      display2();
      break;
    }
  }
}
