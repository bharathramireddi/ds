#include <stdio.h>

#define MAXSIZE 5

void push(void);

int pop(void);

void display(void);

int ch, top = -1;

int stack[MAXSIZE];

int main() {

  printf("STACK OPERATION\n");

  while (1)

  {

    printf("1-->    PUSH\n");

    printf("2-->    POP\n");

    printf("3-->    DISPLAY\n");

    printf("4-->    EXIT\n\n\n");

    printf("Enter your choice\n");

    scanf("%d", &ch);

    switch (ch)

    {

    case 1:

      push();

      break;

    case 2:

      pop();

      break;

    case 3:

      display();

      break;

    case 4:

      printf("\n Program Exited Sucessfully\n");
      exit(1);
    }
  }
}

void push()

{

  int num;

  if (top == MAXSIZE - 1)

  {

    printf("Stack is Full\n");

    return;

  }

  else

  {

    printf("Enter the element to be pushed\n");

    scanf("%d", &num);

    top = top + 1;

    stack[top] = num;
  }

  return;
}

int pop()

{

  int num;

  if (top == -1)

  {

    printf("Stack is Empty\n");

  }

  else

  {

    num = stack[top];

    printf("poped element is = %d\n", stack[top]);

    top = top - 1;
  }
}

void display()

{

  int i;

  if (top == -1)

  {

    printf("Stack is empty\n");

  }

  else

  {

    printf("\n The Stack is \n");

    for (i = top; i >= 0; i--)

    {

      printf("%d\n", stack[i]);
    }
  }

  printf("\n");
}
