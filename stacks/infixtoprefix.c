#include <stdio.h>
#include <string.h>
char stack[20];
int top = -1;
void push(char x) { stack[++top] = x; }

char pop() {
  if (top == -1)
    return -1;
  else
    return stack[top--];
}

int priority(char x) {
  if (x == '(')
    return 0;
  if (x == '+' || x == '-')
    return 1;
  if (x == '*' || x == '/')
    return 2;
}

main() {
  char exp[20], pre[20], rev[20];
  char *e, x;
  int k, i, j = 0, len;
  printf("Enter the expression :: ");
  scanf("%s", exp);
  len = strlen(exp);
  for (i = len - 1; i >= 0; i--) {
    rev[j] = exp[i];
    j++;
  }
  e = rev;
  printf("%s", e);
  while (*e != '\0') {
    if (isalnum(*e)) {
      pre[k] = *e;
      k++;
    } else if (*e == ')') {
      push(*e);
    } else if (*e == '(') {
      while ((x = pop()) != ')')
        pre[k] = x;
      k++;
    } else {
      while (priority(stack[top]) >= priority(*e)) {
        pre[k] = pop();
        k++;
      }
      push(*e);
    }
    e++;
  }
  while (top != -1) {
    pre[k] = pop();
    k++;
  }
  pre[k] = '\0';
  printf("\n \n Prefix : ");
  for (i = len - 1; i >= 0; i--) {
    printf("%c", pre[i]);
  }
}
